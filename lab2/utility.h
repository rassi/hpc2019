void timer_ss(int runs);
void parser();

#define INV_NANO 1000000000

inline void parser(int argc, char **argv)
{
  char *lvalue = NULL;
  char *tvalue = NULL;
//  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "l:t:")) != -1)
    switch (c)
      {
      case 'l':
        lvalue = optarg;
        break;
      case 't':
        tvalue = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        exit(0);
      default:
        abort ();
      }

//  printf ("lvalue = %s\ntvalue = %s\n", lvalue, tvalue);

//  for (index = optind; index < argc; index++)
//    printf ("Non-option argument %s\n", argv[index]);

  nr_threads = atoi(tvalue);
  nr_lines = atoi(lvalue);
  strcpy(nr_lines_string, lvalue);
  degree = atoi(argv[optind]);
}

inline void timer_ss(int runs)
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((INV_NANO * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / runs;
                printf("Sec : %lu\nNano: %lu\n", time / INV_NANO, time % INV_NANO);
                select = 1;
        }
}

