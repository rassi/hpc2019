#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(){
	for(int d = 1; d <= 9; ++d)
	{
		printf("Roots for d = %i\n", d);
		for(int n = 1; n <= d; ++n)
		{
			double complex root = cexp(I*M_PI*2.0*((double)n)/((double)d));
			printf("%.15f,%.15f,\n", creal(root)*creal(root), cimag(root)*cimag(root));
		}
		printf("};");
	}
	return 0;
}
