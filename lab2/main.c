#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> //uint64_t
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <pthread.h>
#include <complex.h>

#include "roots.h"

//Include utility.h without c for some heavy direct inlining.

#define LIMIT 0.001
#define LIMIT_LIMIT 0.000001
#define CON_ABS 10000000000
#define PENALTY 10
#define NUMBER_OF_RUNS 1
#define MAX_LINES 100000
#define LOG10_MAX_LINES 5
#define QUALITY_CAP 50

uint32_t nr_threads, nr_lines, degree, current_line;
char nr_lines_string[LOG10_MAX_LINES];
#include "utility.h"

//Max size on heap right now 10^6
//x_{k+1} = x_k - f(x_k) / f’(x_k) where f(x) = x^d - 1 and f'(x) = d*x^(d-1)
//358 iterations is max or 67211 when pol 4 and lines 3000

void program();
void *parallel_task(void *threadid);
void f_x(double complex z, double imag, double imag2, uint64_t row, uint32_t col);
void write_to_file();

uint8_t * line_done_p, * attractors, * convergences, * attractor_s, * convergence_s;
uint32_t sizeof_att_s, sizeof_con_s;
long unsigned int * threads;

int main (int argc, char **argv)
{
	timer_ss(NUMBER_OF_RUNS);

	//PARSE ARGUMENTT
	parser(argc, argv);
	printf("L=%i, T=%i, D=%i\n", nr_lines, nr_threads, degree);

	for (int i = 0; i < NUMBER_OF_RUNS; i++){

	line_done_p = (uint8_t*)calloc(nr_lines, sizeof(uint8_t));
	threads = (long unsigned int*)malloc(nr_threads * sizeof(long unsigned int));
	attractors = (uint8_t*)malloc(nr_lines*nr_lines * sizeof(uint8_t));
	convergences = (uint8_t*)malloc(nr_lines*nr_lines * sizeof(uint8_t));
	sizeof_att_s = 6*nr_lines;
	sizeof_con_s = 12*nr_lines;
	attractor_s = (uint8_t*)malloc(sizeof_att_s * sizeof(uint8_t));
	convergence_s = (uint8_t*)malloc(sizeof_con_s * sizeof(uint8_t));

		program();
	//STOP TIMER
	free(line_done_p);
        free(threads);
	free(attractors);
	free(convergences);
	free(attractor_s);
	free(convergence_s);
	}
	timer_ss(NUMBER_OF_RUNS);
}

inline void program()
{
	current_line = 0;
        if ((line_done_p == NULL) || (threads == NULL)) {
	printf("Out of memory\n");
        exit(-1);
        }

	for(uint32_t i = 0; i < nr_threads; ++i)
	{
		if(pthread_create(&threads[i], NULL, parallel_task, (void *)i))
		{
			printf("ERROR: pthread_create(); failed\n");
			exit(-1);
		}
		//pthread_detach(threads[i]);
		//printf("MASTER CREATED: %li\n", threads[i]);
	}
	//Main thread do the write to file
	write_to_file();
	for(uint32_t t = 0; t < nr_threads; ++t)
		pthread_join(threads[t], NULL);
}

pthread_mutex_t lock1, lock2;

inline void *parallel_task(void *threadid)
{
	uint32_t my_line;
	//uint32_t my_id = (uint32_t)threadid;
	//nanosleep((const struct timespec[]){{0, 0}}, NULL); //s,ns
	while(1)
	{
		//GET NEW LINE OR FINNISH
		pthread_mutex_lock(&lock1);
		if(current_line < nr_lines)
		{
			my_line = current_line;
			current_line++;
			pthread_mutex_unlock(&lock1);
		}
		else
		{
			pthread_mutex_unlock(&lock1);
			break;
		}
		//WORK
		uint64_t row_v = nr_lines*my_line;
		double complex img_p = (-2 + (((double)(4*my_line))/((double)(nr_lines-1))))*I;
		double imag = cimag(img_p);
		double imag2 = imag*imag;
		for(uint32_t i = 0; i < nr_lines; ++i)
			f_x(-2 + (((double)(4*i))/((double)(nr_lines-1))) + img_p, imag, imag2, row_v, i);

		//SIGNAL LINE DONE
		line_done_p[my_line] = 1;
	}
	//printf("THREAD EXIT: %i\n", my_id);
	pthread_exit(NULL);
}

inline void f_x(double complex z, double imag, double imag2, uint64_t row, uint32_t col)
{
	//printf("Current Z is R: %.4f I: %.4f\n", creal(z), cimag(z));
	uint64_t convergence = 0, i;
	double complex z2, z4;
	double real = creal(z);
	double real2 = real*real;
	do{
		//CHECK IF NEAR 0
		if (LIMIT_LIMIT > real2 + imag2
		|| imag > CON_ABS || -CON_ABS > real
		|| real > CON_ABS || -CON_ABS > real)
		{
			attractors[row+col] = 0;
			if(convergence > 49)
			{
				convergences[row+col] = 49;
				return;
			}
			convergences[row+col] = (uint8_t)convergence;
			return;
		}
		switch (degree)
		{
			case 1:
			z = 1.0;
			break;
			case 2:
			z = (z*z+1.0)/(z*2.0);
			break;
			case 3:
			z = (1.0/(z*z*3.0)) + (z*2.0/3.0);
                	break;
			case 4:
			z = (1.0/(z*z*z*4.0)) + (z*3.0/4.0);
                	break;
			case 5:
			z2 = z*z;
			z = (1/(z2*z2*5.0)) + (z*4.0/5.0);
                	break;
			case 6:
			z2 = z*z;
			z = (1.0/(z2*z2*z*6.0)) + (z*5.0/6.0);
                	break;
			case 7:
			z2 = z*z;
			z = ((1.0/7.0)/(z2*z2*z2)) + (z*(6.0/7.0));
                	break;
			case 8:
			z2 = z*z;
			z = (1/(z2*z2*z2*z*8.0)) + (z*7.0/8.0);
                	break;
			case 9:
			z2 = z*z;
			z4 = z2*z2;
			z = (1/(z4*z4*9.0)) + (z*8.0/9.0);
                	break;
			default:
			fprintf(stderr, "unexpected degree\n");
			exit(1);
		}
		real = creal(z);
        	imag = cimag(z);
		for(i = 0; i < degree; ++i) //MOVE THIS AFTER CHECK IF NEAR 0?
		{
			if( (real-rd[degree-1][i*2])*(real-rd[degree-1][i*2]) +
			((imag-rd[degree-1][i*2+1])*(imag-rd[degree-1][i*2+1])) < LIMIT_LIMIT)
			//if(real2 + rpd[degree-1][i*2] - real*(2.0*rd[degree-1][i*2]) + 
			//imag2 + rpd[degree-1][i*2+1] - imag*(2.0*rd[degree-1][i*2+1]) < LIMIT_LIMIT)
			{
				//printf("CONVERGED!!!\n");
				attractors[row+col] = i+1;
				if(convergence > 49)
				{
					convergences[row+col] = 49;
					return;
				}
				convergences[row+col] = (uint8_t)convergence;
				return;
			}
		}
	real2 = real*real;
        imag2 = imag*imag;
	//printf("Z: %.15f, %.15f*I\n", creal(z), cimag(z));
		convergence++;
	} while(1);
}
char grey_lut[] = "0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950";
inline void write_to_file()
{
	FILE * fp_attractor;
	FILE * fp_convergence;
	char name1[] = "newton_attractors_xD.ppm";
	name1[19] = '0' + degree;
	char name2[] = "newton_convergence_xD.ppm";
	name2[20] = '0' + degree;
	fp_attractor = fopen (name1, "w");
	fp_convergence = fopen (name2, "w");
	//WRITE HEADER TO BOTH FILES
	fprintf(fp_attractor, "%s%c%s %s%c%s%c", "P3", '\n', nr_lines_string, nr_lines_string, '\n', "9", '\n');
	fprintf(fp_convergence, "%s%c%s %s%c%s%c", "P3", '\n', nr_lines_string, nr_lines_string, '\n', "100", '\n');

	//char attractor_s[sizeof_att_s];
	//char convergence_s[sizeof_con_s];

	//WRITE FILE FOR EACH LINE
	for(int n = 0; n < nr_lines; ++n)
	{
		//WAIT FOR WORKER FINNISHED WRITING LINE TO MATRIX
		while(!line_done_p[n]) {
			nanosleep((const struct timespec[]){{0, 100}}, NULL);
		}
		uint64_t row = n*nr_lines;
		//PRINT 1 LINE TO FILE
		for(int m = 0; m < nr_lines; ++m)
		{
			uint64_t rowcol = row + m;
			attractor_s[m*6] = '0';
			attractor_s[m*6+2] = '0';
			attractor_s[m*6+4] = '0' + attractors[rowcol];
			attractor_s[m*6+1] = ' ';
			attractor_s[m*6+3] = ' ';
			attractor_s[m*6+5] = ' ';
			convergence_s[m*12] = '0'; //grey_lut[convergences[rowcol]*2];
			convergence_s[m*12+1] = grey_lut[convergences[rowcol]*2];
			convergence_s[m*12+2] = grey_lut[convergences[rowcol]*2+1];
			convergence_s[m*12+3] = ' ';
			convergence_s[m*12+4] = '0'; //grey_lut[convergences[rowcol]*2];
                        convergence_s[m*12+5] = grey_lut[convergences[rowcol]*2];
                        convergence_s[m*12+6] = grey_lut[convergences[rowcol]*2+1];
                        convergence_s[m*12+7] = ' ';
			convergence_s[m*12+8] = '0'; //grey_lut[convergences[rowcol]*2];
                        convergence_s[m*12+9] = grey_lut[convergences[rowcol]*2];
                        convergence_s[m*12+10] = grey_lut[convergences[rowcol]*2+1];
                        convergence_s[m*12+11] = ' ';
			//array[0] = '0' + (n - (n % 100)) / 100;
			//array[1] = '0' + ((n % 100) - (n % 10))/10;
			//array[2] = '0' + (n % 10);
		}
		attractor_s[sizeof_att_s-1] = '\n';
		convergence_s[sizeof_con_s-1] = '\n';
		fwrite(attractor_s, sizeof(char), sizeof_att_s, fp_attractor);
		fwrite(convergence_s, sizeof(char), sizeof_con_s, fp_convergence);
	}
	fclose(fp_attractor);
	fclose(fp_convergence);
}

