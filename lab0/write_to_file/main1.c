#define SIZE 10000000000

#include <stdio.h>
#include <stdlib.h>

int main()
{
   FILE *fp;
  
   fp = fopen("sq_matrix","r");
   int buffer[100];
   fread(buffer, 100, 4, fp);
   for(int i = 0; i != 100; i++){
      printf("%i", buffer[i]);
      if(((i + 1) % 10) == 0)
         printf("\n");
      else
         printf("%c", ' ');
   }
   fclose(fp);
   
   return(0);
}
