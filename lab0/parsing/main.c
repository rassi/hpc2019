//Source of inspiration: https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html#Example-of-Getopt

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char **argv)
{

  int aflag = 0;
  int bflag = 0;
  char *avalue = NULL;
  char *bvalue = NULL;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "a:b:")) != -1)
    switch (c)
      {
      case 'a':
        aflag = 1;
	avalue = optarg;
        break;
      case 'b':
        bflag = 1;
	bvalue = optarg;
        break;
	default:
          abort ();
      }
  if((aflag == 1) && (bflag == 1))
	{
	  int a = atoi(avalue);
	  int b = atoi(bvalue);
	  printf("A is %i and B is %i\n", a, b);
	}
	else
		printf("ERROR: not correct arguments!\n");
}
