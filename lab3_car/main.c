#include <omp.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

#define DISTANCES 3465
#define MAX_PROGRAM_MEM 67108864 //2^26
#define MAX_MEM (16777216/128) //2^24
#define HALF_MAX_MEM (8388608/128)
#define NR_TIMES 10
//#define DEBUG_MODE


uint16_t nr_threads;

void timer_ss();
void parser(int argc, char **argv);

int main(int argc, char **argv) {

	//GET COMMANDS
	parser(argc, argv);
	//SET NUMBER OF THREADS
        omp_set_num_threads(nr_threads);

	//START TIMER
	#ifdef NR_TIMES
	timer_ss(0);
	for(uint16_t times=0; times < NR_TIMES; times++){
	#endif

	//GET  NUMBER OF CELLS
	FILE *fp;
	fp = fopen("cells","r"); //Open file cells
	fseek(fp, 0, SEEK_END); //Go to end
	uint32_t nr_cells = ftell(fp)/24; //Get current file pointer
	fseek(fp, 0, SEEK_SET); //Return to start

	//printf("Cells: %i\n", nr_cells);
	uint32_t nr_blocks = sizeof(float)*3*nr_cells/HALF_MAX_MEM + 1;
	uint32_t block_size;
	float *data;
	if (MAX_MEM < sizeof(float)*3*nr_cells)
	{
		data = (float *)malloc(MAX_MEM);
		block_size = HALF_MAX_MEM/(sizeof(float)*3);
		nr_blocks = sizeof(float)*3*nr_cells/HALF_MAX_MEM + 1;
	}
	else
	{
		data = (float *)malloc(sizeof(float)*3*nr_cells); //Memory for all the cells
		block_size = nr_cells;
		nr_blocks = 1;
	}
	#ifdef DEBUG_MODE
	printf("BLOCK SIZE: %i, NR_BLOCKS: %i\n", block_size, nr_blocks);
	#endif
	char oc[24]; //One Cell String
	float x,y,z,x1,y1,z1; //Temporary Variables
	uint32_t i,j,m,n; //Iteration Ints
	uint64_t dc[DISTANCES]; for(i=0; i<DISTANCES; ++i) dc[i]=0; //Distance Counter Array
	long i1;
	const float threehalfs = 1.5F;
	//printf("Mem: %i\n", nr_cells*4*sizeof(float));

	//CALCULATE ALL DISTANCES
	for(m = 0; m < nr_blocks; ++m)
	{
		//SET FILE POINTER TO START OF CELL CHUNK WITH OFFSET M
		long int fp_pos = m*block_size*24;
		fseek(fp, 0, SEEK_SET);
		fseek(fp, fp_pos, SEEK_CUR);
		//LIMIT THE READ SIZE
		uint32_t read_size = block_size;
		if(m==nr_blocks-1) read_size = nr_cells%block_size;
		if(read_size == 0) read_size = block_size; //Special case for read_size==0
		//READ DATA TO MEMORY A
		for(uint32_t i = 0; i < read_size; ++i)
		{
			fread(oc, 1, 24, fp);
			data[i*3]=(','-oc[0])*((oc[1]-'0')*10.0+(oc[2]-'0')+(oc[4]-'0')*0.1+(oc[5]-'0')*0.01+(oc[6]-'0')*0.001);
			data[i*3+1]=(','-oc[8])*((oc[9]-'0')*10.0+(oc[10]-'0')+(oc[12]-'0')*0.1+(oc[13]-'0')*0.01+(oc[14]-'0')*0.001);
			data[i*3+2]=(','-oc[16])*((oc[17]-'0')*10.0+(oc[18]-'0')+(oc[20]-'0')*0.1+(oc[21]-'0')*0.01+(oc[22]-'0')*0.001);
		}

		//MEMORY A. RUN FOR A->A FOR ALL A-1
		#pragma omp parallel for shared(i) private(j,x,y,z,x1,y1,z1,i1) reduction(+:dc[:DISTANCES])
		for(i=0; i<read_size-1; ++i)
		{
			x=data[i*3]; y=data[i*3+1]; z=data[i*3+2];
			for(j=i+1; j<read_size; ++j)
			{
				x1=data[j*3]; y1=data[j*3+1]; z1=data[j*3+2];
				x1=x-x1; x1*=x1; y1=y-y1; y1*=y1; z1=z-z1; z1*=z1;
				z1=z1+y1+x1;
				x1 = z1 * 0.5;
				y1  = z1;
				i1  = * ( long * ) &y1;                       // evil floating point bit level hacking
				i1  = 0x5f3759df - ( i1 >> 1 );               // what the fuck? 
				y1  = * ( float * ) &i1;
				y1  = y1 * ( threehalfs - ( x1 * y1 * y1 ) );
				dc[(uint16_t)(0.5+100/y1)]++;
			}
		}

		//MEMORY B. RUN FOR A->B FOR ALL A AND B
		for(n=m+1; n < nr_blocks; ++n)
		{
			//LIMIT THE READ SIZE
			if(n==nr_blocks-1) read_size = nr_cells%block_size;
			if(read_size == 0) read_size = block_size;
			//READ DATA TO MEMORY B
			for(uint32_t i = block_size; i < block_size+read_size; ++i)
	                {
	                        fread(oc, 1, 24, fp);
	                        data[i*3]=(','-oc[0])*((oc[1]-'0')*10.0+(oc[2]-'0')+(oc[4]-'0')*0.1+(oc[5]-'0')*0.01+(oc[6]-'0')*0.001);
	                        data[i*3+1]=(','-oc[8])*((oc[9]-'0')*10.0+(oc[10]-'0')+(oc[12]-'0')*0.1+(oc[13]-'0')*0.01+(oc[14]-'0')*0.001);
	                        data[i*3+2]=(','-oc[16])*((oc[17]-'0')*10.0+(oc[18]-'0')+(oc[20]-'0')*0.1+(oc[21]-'0')*0.01+(oc[22]-'0')*0.001);
	                }
			//MEMORY A. RUN FOR A->B FOR ALL A
			#pragma omp parallel for shared(i) private(j,x,y,z,x1,y1,z1,i1) reduction(+:dc[:DISTANCES])
			for(i=0; i<block_size; ++i)
			{
				x=data[i*3]; y=data[i*3+1]; z=data[i*3+2];
				for(j=block_size; j<block_size+read_size; ++j)
				{
					x1=data[j*3]; y1=data[j*3+1]; z1=data[j*3+2];
                                	x1=x-x1; x1*=x1; y1=y-y1; y1*=y1; z1=z-z1; z1*=z1;
					z1=z1+y1+x1;
	                                x1 = z1 * 0.5;
	                                y1  = z1;
	                                i1  = * ( long * ) &y1;                       // evil floating point bit level hacking
	                                i1  = 0x5f3759df - ( i1 >> 1 );               // what the fuck?
	                                y1  = * ( float * ) &i1;
	                                y1  = y1 * ( threehalfs - ( x1 * y1 * y1 ) );
	                                dc[(uint16_t)(0.5+100/y1)]++;

				}
			}
		}
	}
	fclose(fp);

	//PRINT DISTANCES
	for(i=0; i<DISTANCES; ++i)
		if(dc[i])
			printf("%c%c.%c%c %i\n", '0'+ i/1000,'0'+ (i/100)%10,'0'+ (i/10)%10,'0'+ i%10, dc[i]);
	//FREE DATA
	free(data);
	//STOP TIMER

	#ifdef NR_TIMES
	}
	timer_ss(NR_TIMES);
	#endif
	return 0;
}

inline void timer_ss(uint32_t nr_times)
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((1000000000 * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec))/nr_times;
                printf("Sec : %lu\nNano: %lu\n", time / 1000000000, time % 1000000000);
                select = 1;
        }
}

inline void parser(int argc, char **argv)
{
  char *lvalue = NULL;
  char *tvalue = NULL;
//  int index;
  int c;

  opterr = 0;

  while ((c = getopt (argc, argv, "t:")) != -1)
    switch (c)
      {
      case 't':
        tvalue = optarg;
        break;
      case '?':
        if (optopt == 'c')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,
                   "Unknown option character `\\x%x'.\n",
                   optopt);
        exit(0);
      default:
        abort ();
      }

//  printf ("lvalue = %s\ntvalue = %s\n", lvalue, tvalue);

//  for (index = optind; index < argc; index++)
//    printf ("Non-option argument %s\n", argv[index]);

  nr_threads = atoi(tvalue);
}
