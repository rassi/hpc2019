#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#define INV_NANO 1000000000

int main(){
	int * as = NULL;
	int sum = 0;

	for ( int ix = 0; ix < 10; ++ix )
		as[ix] = ix;

	for ( int ix = 0; ix < 10; ++ix )
		sum += as[ix];

	free(as);
}

void timer_ss()
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
		timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = (INV_NANO * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec);
                printf("Sec : %lu\nNano: %lu\n", time / INV_NANO, time % INV_NANO);
                select = 1;
        }
}

