#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#define inv_nano 1000000000

void mul_cpx_mainfile( double * a_re, double * a_im, double * b_re, double * b_im, double * c_re, double * c_im);

#define NUMBER_OF_RUNS 100
void timer_ss();

int main ()
{
	//START TIMER

	//PROGRAM
	volatile double zabc[6][30000];
	for(int i = 0; i != 30000; i++)
	{
		zabc[2][i] = ((double)rand() / 1000);
		zabc[3][i] = ((double)rand() / 1000);
		zabc[4][i] = ((double)rand() / 1000);
		zabc[5][i] = ((double)rand() / 1000);
	}

	timer_ss();

	for (int i = 0; i < NUMBER_OF_RUNS; i++){

	for(int i = 0; i != 30000; i++)
		mul_cpx_mainfile(&zabc[0][i], &zabc[1][i], &zabc[2][i], &zabc[3][i], &zabc[4][i], &zabc[5][i]);
	}

	timer_ss();

//	for(int i = 0; i != 3000; i++)
//		printf("RE: %f, IM: %f\n", zabc[0][i], zabc[1][i]);

}

void mul_cpx_mainfile( double * a_re, double * a_im, double * b_re, double * b_im, double * c_re, double * c_im)
{
        *a_re = *b_re * *c_re - *b_im * *c_im;
        *a_im = *b_re * *c_im + *c_re * *b_im;
}

void timer_ss()
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / NUMBER_OF_RUNS;
                printf("Sec : %lu\nNano: %lu\n", time / inv_nano, time % inv_nano);
                select = 1;
        }
}
