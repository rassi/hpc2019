#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "separate.h"

#define inv_nano 1000000000

#define NUMBER_OF_RUNS 1000
void timer_ss();

int main ()
{
        //START TIMER
        struct timespec ts_start, ts_stop;
        timespec_get(&ts_start, TIME_UTC);
        //END OF START TIMER

        //PROGRAM
        volatile double zabc[6][30000];
        for(int i = 0; i != 30000; i++)
        {
                zabc[2][i] = ((double)rand() / 1000);
                zabc[3][i] = ((double)rand() / 1000);
                zabc[4][i] = ((double)rand() / 1000);
                zabc[5][i] = ((double)rand() / 1000);
        }

        timer_ss();

        for (int i = 0; i < NUMBER_OF_RUNS; i++){

        for(int i = 0; i != 30000; i++)
                mul_cpx_mainfile(&zabc[0][i], &zabc[1][i], &zabc[2][i], &zabc[3][i], &zabc[4][i], &zabc[5][i]);
        }

        timer_ss();
}

void timer_ss()
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / NUMBER_OF_RUNS;
                printf("Sec : %lu\nNano: %lu\n", time / inv_nano, time % inv_nano);
                select = 1;
        }
}

