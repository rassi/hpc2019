#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#define inv_nano 1000000000
#define P2T20 1048576
#define NUMBER_OF_RUNS 10

void timer_ss();

void main(){
        uint32_t numbers[P2T20];
        for(uint32_t i = 0; i < P2T20; i++)
                numbers[i] = i;
	FILE * fp;

	printf("WRITE TO HDD\n");
	timer_ss();
	for (int i = 0; i < NUMBER_OF_RUNS; i++){
	fp = fopen ("sq_matrix", "w");
	for(int number = 0; number != 100; number++)
		fwrite(numbers, sizeof(uint32_t), P2T20, fp);
   	fclose(fp);
	}
	timer_ss();

	printf("WRITE TO SSD\n");
	timer_ss();
	for (int i = 0; i < NUMBER_OF_RUNS; i++){
	fp = fopen ("/run/mount/scratch/hpcuser045/sq_matrix", "w");
        for(int number = 0; number != 100; number++)
                fwrite(numbers, sizeof(uint32_t), P2T20, fp);
        fclose(fp);
	}
	timer_ss();

	printf("READ FROM HDD\n");
        timer_ss();
	for (int i = 0; i < NUMBER_OF_RUNS; i++){
	fp = fopen("sq_matrix","r");
	fread(numbers, P2T20, sizeof(uint32_t), fp);
	}
	fclose(fp);
        timer_ss();

	printf("READ FROM SDD\n");
        timer_ss();
	for (int i = 0; i < NUMBER_OF_RUNS; i++){
	fp = fopen("/run/mount/scratch/hpcuser045/sq_matrix","r");
        fread(numbers, P2T20, sizeof(uint32_t), fp);
        fclose(fp);
        }
	timer_ss();
}

void timer_ss()
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / NUMBER_OF_RUNS;
                printf("Sec : %lu\nNano: %lu\n", time / inv_nano, time % inv_nano);
                select = 1;
        }
}

