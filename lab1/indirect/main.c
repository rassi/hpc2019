#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#define inv_nano 1000000000

#define SIZE_V 1000000
#define NUMBER_OF_RUNS 100

void timer_ss();

int main()
{
	//SETUP
	uint32_t * x, * y, * p;
	x = (uint32_t *) malloc(sizeof(uint32_t) * SIZE_V);
	y = (uint32_t *) malloc(sizeof(uint32_t) * SIZE_V);
	p = (uint32_t *) malloc(sizeof(uint32_t) * SIZE_V);
	uint32_t m, n, a, ix, jx, kx;

	//START TIMER
        printf("RUN INDIRECT ACCESSING\n");
        timer_ss();

	//RUN 1000 TIMES
	for (int i = 0; i < NUMBER_OF_RUNS; i++){
	m = 1000;
	a = 1;
	ix = 0;
	n = 1000000;

	for (jx=0; jx < m; ++jx)
		for (kx=0; kx < m; ++kx)
			p[jx + m*kx] = ix++;
        for (kx=0; kx < n; ++kx) {
                jx = p[kx];
                y[jx] += a * x[jx];
        }
	}

	//STOP TIMER
	timer_ss();

	//START TIMER
	printf("RUN DIRECT ACCESSING\n");
        timer_ss();

	//RUN 1000 TIMES
        for (int i = 0; i < NUMBER_OF_RUNS; i++){
        m = 1000;
        a = 1;
        ix = 0;
        n = 1000000;
	for (jx=0; jx < m; ++jx)
                for (kx=0; kx < m; ++kx)
                        p[jx + m*kx] = ix++;
	
	//PROGRAM
	for (kx=0; kx < SIZE_V; ++kx) {
                y[kx] += a * x[kx];
        }
	}

	//STOP TIMER
        timer_ss();

	free(x); free(y); free(p);
}

void timer_ss()
{
	static struct timespec ts_start, ts_stop;
	static char select = 1;
	if(select)
	{
		select = 0;
		timespec_get(&ts_start, TIME_UTC);
	}
	else
	{
		timespec_get(&ts_stop, TIME_UTC);
        	uint64_t time = ((inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / NUMBER_OF_RUNS;
        	printf("Sec : %lu\nNano: %lu\n", time / inv_nano, time % inv_nano);
		select = 1;
	}
}
