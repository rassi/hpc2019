#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#define inv_nano 1000000000
#define m_nrs 1000
#define m_ncs 1000

#define NUMBER_OF_RUNS 2000

void timer_ss();
void row_sums(double * sums, const double ** matrix, size_t nrs, size_t ncs);
void col_sums(double * sums, const double ** matrix, size_t nrs, size_t ncs);

int main()
{
	double mat[1000000];
	for(int i = 0; i < 1000000; i++)
		mat[i] = 0;
	double *matrix1[1000];
	for(int i = 0; i < 1000; i++)
		matrix1[i] = &mat[i * 1000];
	const double ** matrix = matrix1;
	double sums[m_nrs];

	//START TIMER
	timer_ss();
	//END OF START TIMER

	
for (int i = 0; i < NUMBER_OF_RUNS; i++){
	//PROGRAM
	row_sums(sums, matrix, m_nrs, m_ncs);
	//END PROGRAM
}

	//STOP TIMER
	timer_ss();
	//END OF STOP TIMER

	for(int i = 0; i < 1000000; i++)
                mat[i] = 0;

	for(int i = 0; i < 1000; i++)
                matrix1[i] = &mat[i * 1000];

	//START TIMER
        timer_ss();
        //END OF START TIMER

	
for (int i = 0; i < NUMBER_OF_RUNS; i++){
        //PROGRAM
        col_sums(sums, matrix, m_nrs, m_ncs);
        //END PROGRAM
	}

        //STOP TIMER
        timer_ss();
        //END OF STOP TIMER


}

void timer_ss()
{
        static struct timespec ts_start, ts_stop;
        static char select = 1;
        if(select)
        {
                select = 0;
                timespec_get(&ts_start, TIME_UTC);
        }
        else
        {
                timespec_get(&ts_stop, TIME_UTC);
                uint64_t time = ((inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec)) / NUMBER_OF_RUNS;
                printf("Sec : %lu\nNano: %lu\n", time / inv_nano, time % inv_nano);
                select = 1;
        }
}

void
row_sums(
  double * sums,
  const double ** matrix,
  size_t nrs,
  size_t ncs
)
{
  for ( size_t ix=0; ix < nrs; ++ix ) {
    double sum = 0;
    for ( size_t jx=0; jx < ncs; ++jx )
	sum += matrix[ix][jx];
    sums[ix] = sum;
  }
}

void
col_sums(
  double * sums,
  const double ** matrix,
  size_t nrs,
  size_t ncs
  )
{
  for ( size_t jx=0; jx < ncs; ++jx ) {
    double sum = 0;
    for ( size_t ix=0; ix < nrs; ++ix )
      sum += matrix[ix][jx];
    sums[jx] = sum;
  }
}
