	.file	"main.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"The sum: %llu\n"
.LC1:
	.string	"Sec: %llu.%llu\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB22:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	subq	$32, %rsp
	.cfi_def_cfa_offset 48
	movl	$1, %esi
	leaq	16(%rsp), %rdi
	call	timespec_get
	movl	$0, %eax
	movl	$0, %ebx
.L2:
	cmpl	$1000000000, %eax
	je	.L5
	movl	%eax, %edx
	addq	%rdx, %rbx
	addl	$1, %eax
	jmp	.L2
.L5:
	movl	$1, %esi
	movq	%rsp, %rdi
	call	timespec_get
	movq	%rbx, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movq	(%rsp), %rax
	subq	16(%rsp), %rax
	imulq	$1000000000, %rax, %rax
	movq	8(%rsp), %rcx
	subq	24(%rsp), %rcx
	addq	%rax, %rcx
	movq	%rcx, %rdx
	shrq	$9, %rdx
	movabsq	$19342813113834067, %rsi
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rax
	subq	%rax, %rcx
	movq	%rcx, %rdx
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	addq	$32, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE22:
	.size	main, .-main
	.ident	"GCC: (GNU) 9.1.1 20190503 (Red Hat 9.1.1-1)"
	.section	.note.GNU-stack,"",@progbits
