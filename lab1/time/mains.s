	.file	"main.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"The sum: %llu\n"
.LC1:
	.string	"Sec: %llu.%llu\n"
	.section	.text.startup,"ax",@progbits
	.globl	main
	.type	main, @function
main:
.LFB6:
	.cfi_startproc
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	movl	$1, %esi
	movq	%rsp, %rdi
	call	timespec_get
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	call	timespec_get
	movl	$.LC0, %edi
	xorl	%eax, %eax
	movabsq	$499999999500000000, %rsi
	call	printf
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	xorl	%edx, %edx
	movl	$1000000000, %ecx
	imulq	$1000000000, %rax, %rax
	addq	24(%rsp), %rax
	subq	8(%rsp), %rax
	divq	%rcx
	movl	$.LC1, %edi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	printf
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE6:
	.size	main, .-main
	.ident	"GCC: (GNU) 9.1.1 20190503 (Red Hat 9.1.1-1)"
	.section	.note.GNU-stack,"",@progbits
