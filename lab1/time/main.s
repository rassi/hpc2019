	.file	"main.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"a::b::"
.LC1:
	.string	"A is %i and B is %i\n"
.LC2:
	.string	"ERROR: not correct arguments!"
	.text
	.globl	main
	.type	main, @function
main:
.LFB24:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	movl	%edi, %ebp
	movq	%rsi, %rbx
	movl	$0, opterr(%rip)
	movl	$0, %r15d
	movl	$0, %r14d
	movl	$0, 12(%rsp)
	movl	$0, %r12d
	movl	$1, %r13d
.L2:
	movl	$.LC0, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	getopt
	cmpl	$-1, %eax
	je	.L12
	cmpl	$97, %eax
	jne	.L13
	movq	optarg(%rip), %r14
	movl	%r13d, %r12d
	jmp	.L2
.L13:
	cmpl	$98, %eax
	jne	.L14
	movq	optarg(%rip), %r15
	movl	%r13d, 12(%rsp)
	jmp	.L2
.L14:
	call	abort
.L12:
	testl	%r12d, %r12d
	je	.L8
	cmpl	$0, 12(%rsp)
	je	.L8
	movl	$10, %edx
	movl	$0, %esi
	movq	%r14, %rdi
	call	strtol
	movq	%rax, %rbx
	movl	$10, %edx
	movl	$0, %esi
	movq	%r15, %rdi
	call	strtol
	movq	%rax, %rdx
	movl	%ebx, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
.L9:
	movl	$0, %eax
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
.L8:
	.cfi_restore_state
	movl	$.LC2, %edi
	call	puts
	jmp	.L9
	.cfi_endproc
.LFE24:
	.size	main, .-main
	.ident	"GCC: (GNU) 9.1.1 20190503 (Red Hat 9.1.1-1)"
	.section	.note.GNU-stack,"",@progbits
