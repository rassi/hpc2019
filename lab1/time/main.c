#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#define inv_nano 1000000000
int main ()
{
	//START TIMER
	struct timespec ts_start, ts_stop;
	timespec_get(&ts_start, TIME_UTC);
	//END OF START TIMER

	//PROGRAM
	uint64_t sum = 0; //volatile
//	for(char y=0;y<10;y++){
//	sum = sum - sum + y;
	for(uint32_t i = 0; i != 1000000000; i++)
		sum += i;
//	}

	//END TIMER
	timespec_get(&ts_stop, TIME_UTC);
	printf("The sum: %llu\n", sum);
	uint64_t time = (inv_nano * (ts_stop.tv_sec - ts_start.tv_sec)) + (ts_stop.tv_nsec - ts_start.tv_nsec);
	printf("Sec: %llu.%llu\n", time / inv_nano, time % inv_nano);
	//END OF END TIMER
}
