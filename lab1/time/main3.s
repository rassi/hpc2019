	.file	"main.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"The sum: %llu\n"
.LC1:
	.string	"Sec: %llu.%llu\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.globl	main
	.type	main, @function
main:
.LFB22:
	.cfi_startproc
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	movl	$1, %esi
	movq	%rsp, %rdi
	call	timespec_get
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	call	timespec_get
	movl	$.LC0, %edi
	xorl	%eax, %eax
	movabsq	$499999999500000000, %rsi
	call	printf
	movq	16(%rsp), %rcx
	subq	(%rsp), %rcx
	movabsq	$19342813113834067, %rsi
	imulq	$1000000000, %rcx, %rcx
	addq	24(%rsp), %rcx
	subq	8(%rsp), %rcx
	movl	$.LC1, %edi
	movq	%rcx, %rdx
	shrq	$9, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rax
	subq	%rax, %rdx
	xorl	%eax, %eax
	call	printf
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE22:
	.size	main, .-main
	.ident	"GCC: (GNU) 9.1.1 20190503 (Red Hat 9.1.1-1)"
	.section	.note.GNU-stack,"",@progbits
