	.file	"main.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"The sum: %llu\n"
.LC1:
	.string	"Sec: %llu.%llu\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB22:
	.cfi_startproc
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	movl	$1, %esi
	leaq	16(%rsp), %rdi
	call	timespec_get
	movl	$1000000000, %eax
.L2:
	subl	$1, %eax
	jne	.L2
	movl	$1, %esi
	movq	%rsp, %rdi
	call	timespec_get
	movabsq	$499999999500000000, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movq	(%rsp), %rcx
	subq	16(%rsp), %rcx
	imulq	$1000000000, %rcx, %rcx
	addq	8(%rsp), %rcx
	subq	24(%rsp), %rcx
	movq	%rcx, %rdx
	shrq	$9, %rdx
	movabsq	$19342813113834067, %rsi
	movq	%rdx, %rax
	mulq	%rsi
	movq	%rdx, %rsi
	shrq	$11, %rsi
	imulq	$1000000000, %rsi, %rax
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE22:
	.size	main, .-main
	.ident	"GCC: (GNU) 9.1.1 20190503 (Red Hat 9.1.1-1)"
	.section	.note.GNU-stack,"",@progbits
